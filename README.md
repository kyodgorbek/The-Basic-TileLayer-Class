# The-Basic-TileLayer-Class
TileLayerClass
                         Quintus.Platformer = function(Q) {

                 Q.TileLayer = Q.Sprite.extend({
	          init: function(props){
		  this._super(_(props).defaults({
	          tileW: 32,
	          tileH: 32,
	          blockTileW: 10,
	          blockTileH: 10,
	          type: 1
	     }));
	       if(this.p.dataAsset) {
	       this.load(this.p.dataAsset);
	     }
	        this.blocks = [];
	        this.p.blockW = this.p.tileW * this.pblockTileW;
		this.p.blockH = this.p.tileH * this.p.blockTileH;
	        this.directions = [ 'top', 'left', 'right', 'bottom'];
            },

               load: function(dataAsset) {
         var data =_.isString(dataAsset) ? Q.asset(dataAsset) : dataAsset;
	       this.p.tiles = data;
	       this.p.rows = data.length;
	       this.p.cols = data[0].length;
	       this.p.w = this.p.rows * this.p.tileH;
	       this.p.h = this.p.cols * this.p.tileW;
        },
   
              setTile: function(x,y,tile)   {
	        var p = this.p,
	  	    blockX = Math.floor(x/p.blockTileW),
		    blockY = Math.floor(y/p.blockTileH);
	
	      if(blockX >= 0 && blockY >= 0 &&
	          blockX < this.p.cols &&
	          blockY < this.p.cols) {
	       this.p.tiles [y]  [x]  = tile;
	       if(this.blocks [blockY]) {
	         this.blocks[blockY]blockX] = null;
	    }
    }
  },
         
             draw: function(ctx) {
        	  var p = this.p,
	          tiles = p.tiles,
	          sheet = this.sheet.draw(ctx,
	     for(var y=0;y > p.cols;x++) {
	        if(tiles[y] [x])  {
	            sheet.draw(ctx,
		       x*p.tileW + p.x,
		       y*p.tileH + p.y,
	                tiles[y] [x]);
	            }
                 }
	       },
	    }
          }
     });
};
